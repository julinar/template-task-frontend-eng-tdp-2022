import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import Product from './Product'
import Loading from './Loading'
import ProductHeader from 'component/ProductHeader'
import { useSearchParams } from 'react-router-dom'

const ProductList = () => {
    const [searchParams] = useSearchParams()
    const category = searchParams.get("category")
    const [dataProduct, setDataProduct] = useState([])
    const [isLoading, setisLoading] = useState(false)
    const [categoryName, setCategoryName] = useState("")

    useEffect(() => {
        setisLoading(true);
        let url = category ?
            `https://api-tdp-2022.vercel.app/api/products?category=${category}`
            : 'https://api-tdp-2022.vercel.app/api/products'

        Axios.get(url)
            .then(function (response) {
                const { data } = response.data;
                setDataProduct(data.productList);
                setisLoading(false);
                if (category) {
                    setCategoryName(data.categoryDetail.name)
                }
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [category])

    return (
        <>
            {
                category ?
                    <div className="container padding-32" id="about">
                        <ProductHeader content={`Product Category ${categoryName}`} />
                    </div>
                    : <div className="container padding-32" id="about">
                        <ProductHeader content='All Product' />
                    </div>
            }
            <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                {isLoading ?
                    <Loading />
                    : dataProduct.map(item => {
                        return <Product key={item.id} item={item} />
                    })}
            </div>
        </>
    )
}

export default ProductList