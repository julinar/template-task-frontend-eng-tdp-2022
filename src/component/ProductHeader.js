import React from 'react'

const ProductHeader = ({ content }) => {
    return (
        <h3 className="border-bottom border-light-grey padding-16">
            {content}
        </h3>
    )
}

export default ProductHeader