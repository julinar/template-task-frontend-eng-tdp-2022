import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import { useParams } from 'react-router-dom'
import Loading from './Loading'

const ProductInformation = () => {
    const [detailProduct, setDetailProduct] = useState({})
    const [isLoading, setisLoading] = useState(false)
    const [holdButton, setHoldButton] = useState(true)
    const [numQuantity, setNumQuantity] = useState(0)
    const [subTotal, setSubTotal] = useState(0)
    const [total, setTotal] = useState(0)
    let params = useParams()
    const isLoggedIn = localStorage.getItem("isLoggedIn");

    const rupiah = (money) => {
        return new Intl.NumberFormat('id-ID',
            { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
        ).format(money);
    }

    useEffect(() => {
        setisLoading(true);
        Axios.get(`https://api-tdp-2022.vercel.app/api/products/${params?.id}`)
            .then(function (response) {
                const { data } = response;
                setDetailProduct(data.data);
                setisLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [params?.id])

    const handleChange = (event) => {
        let quantityValue = event.target.value;

        if (detailProduct.stock === 0) {
            quantityValue = 0
        }

        if (quantityValue !== 0){
            setHoldButton(false)
        }
        setNumQuantity(quantityValue)
    }

    useEffect(() => {
        setSubTotal(
            (numQuantity * detailProduct.price) - (numQuantity * detailProduct.price * detailProduct.discount)/100
        )
        setTotal(
            numQuantity * detailProduct.price
        )
    }, [numQuantity, detailProduct.price, detailProduct.discount])

    const submitButton = () =>{
        if (isLoggedIn) {
            alert('Product added to cart successfully')
            window.location.replace('/')
        } else {
            window.location.replace('/login')
        }
    }

    return (
        <>
            {isLoading ?
                <Loading />
                : <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                    <div className="col l3 m6 margin-bottom">
                        <div className="product-tumb">
                            <img src={detailProduct.image} alt="Product 1" />
                        </div>
                    </div>
                    <div className="col m6 margin-bottom">
                        <h3>{detailProduct.title}</h3>
                        <div style={{ marginBottom: '32px' }}>
                            <span>Category : <strong>{detailProduct.category}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Review : <strong>{detailProduct.rate}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Stock : <strong>{detailProduct.stock}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Discount : <strong>{`${detailProduct.discount} %`}</strong></span>
                        </div>
                        <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                            {rupiah(detailProduct.price)}
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            {detailProduct.description}
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            <div><strong>Quantity : </strong></div>
                            <input type="number" className="input section border" name="total" min="0" max={detailProduct.stock} placeholder='Quantity' value={numQuantity} onChange={handleChange}/>
                        </div>
                        <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                            {
                                `Sub Total : ${rupiah(subTotal)}`
                            }
                            {
                                detailProduct.discount > 0 ?
                                    <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>{rupiah(total)}</strong></span>
                                    : ""
                            }
                        </div>
                        {
                            detailProduct.stock > 0 ?
                                <button className='button light-grey block' onClick={submitButton} disabled={holdButton}>Add to cart</button>
                                : <button className='button light-grey block' disabled={true}>Empty Stock</button>
                        }
                    </div>
                </div>
            }
        </>
    )
}

export default ProductInformation