import React from 'react'
import map from 'assets/img/map.jpg';

const ImageLocation = () => {
    return (
        <div className="container padding-32">
            <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
        </div>
    )
}

export default ImageLocation