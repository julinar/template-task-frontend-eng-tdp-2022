import React from 'react'

const Product = ({ item }) => {

    let url = `/shop/${item.id}`

    const rupiah = (money) => {
        return new Intl.NumberFormat('id-ID',
          { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
        ).format(money);
    }

    return (
        <div className="product-card">
            {
                item.discount > 0 ?
                    <div className="badge">Discount</div>
                    : ""
            }
            <div className="product-tumb">
                <img src={item.image} alt="product1" />
            </div>
            <div className="product-details">
                <span className="product-catagory">{item.category}</span>
                <h4>
                    <a href={url}>{item.title}</a>
                </h4>
                <p>{item.description}</p>
                <div className="product-bottom-details">
                    <div className="product-price">{rupiah(item.price)}</div>
                    <div className="product-links">
                        <a href={url}>View Detail<i className="fa fa-heart"></i></a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Product