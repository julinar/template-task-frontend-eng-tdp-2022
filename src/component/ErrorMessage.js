import React from 'react'

const ErrorMessage = ({ item }) => {
    return (
        <div style={{ color: '#EF144A' }}>{item}</div>
    )
}

export default ErrorMessage