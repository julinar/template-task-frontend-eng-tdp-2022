import React from 'react'

const Category = ({ item }) => {
    return (
        <div className="col l3 m6 margin-bottom">
            <a href={`/shop?category=${item.id}`}>
                <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                    <div className="display-topleft black padding">{item.name}</div>
                    <img src={item.image} alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                </div>
            </a>
        </div>
    )
}

export default Category