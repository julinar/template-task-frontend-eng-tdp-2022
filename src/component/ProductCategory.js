import React, { useEffect, useState } from 'react'
import Category from './Category';
import Axios from 'axios'

const ProductCategory = () => {

    const [categoryList, setCategoryList] = useState([])

    useEffect(() => {
        Axios.get(`https://api-tdp-2022.vercel.app/api/categories`)
            .then(function (response) {
                const { data } = response;
                let categoryData = data.data;
                setCategoryList(categoryData);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    return (
        <>
            <div className="container padding-32" id="projects">
                <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
            </div>

            <div className="row-padding">
                { categoryList.map(item => {
                    return <Category key={item.id} item={item} />
                })}
            </div>
        </>
    )
}

export default ProductCategory