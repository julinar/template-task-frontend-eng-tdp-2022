import React from 'react'
import Banner from 'component/Banner';
import ProductCategory from 'component/ProductCategory';
import ImageLocation from 'component/ImageLocation';

const Homepage = () => {
    return (
        <>
            <Banner />
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <ProductCategory />
                <ImageLocation />
            </div>
        </>
    )
}

export default Homepage