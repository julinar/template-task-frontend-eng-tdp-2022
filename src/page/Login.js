import ProductHeader from 'component/ProductHeader'
import React, { useState } from 'react'
import Axios from 'axios'

const Login = () => {
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    const handleUsername = (e) => {
        const value = e?.target?.value;
        setUsername(value);
    }

    const handlePassword = (e) => {
        const value = e?.target?.value;
        setPassword(value);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        setIsLoading(true);
        Axios.post('https://api-tdp-2022.vercel.app/api/login', { username, password })
            .then(
                function (response) {
                const { data } = response;
                localStorage.setItem('token', data.data.access_token);
                localStorage.setItem('user_info', username);
                localStorage.setItem("isLoggedIn", true);
                setIsLoading(false);
                window.location.replace("/");
            })
            .catch(function (error) {
                alert(error?.response?.data?.message);
                setIsLoading(false);
            });
    }

    return (
        <>
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="contact" >
                    <ProductHeader content={'Login'} />
                    <p>Lets get in touch and talk about your next project.</p>
                    <form onSubmit={handleSubmit}>
                        <input className="input border" type="text" placeholder="Username" required name="username" onChange={handleUsername} />
                        <input className="input section border" type="password" placeholder="Password" required name="Password" onChange={handlePassword} />
                        <button className="button black section" type="submit" disabled={isLoading}>
                            <i className="fa fa-paper-plane" />
                            {isLoading ? 'Loading' : 'Login'}
                        </button>
                    </form>
                </div >
            </div>
        </>
    )
}

export default Login