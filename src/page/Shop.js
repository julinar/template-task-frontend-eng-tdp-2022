import ProductList from 'component/ProductList'
import React from 'react'

const Shop = () => {
    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <ProductList />
        </div>
    )
}

export default Shop