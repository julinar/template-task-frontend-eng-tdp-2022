import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import ErrorMessage from 'component/ErrorMessage'

const Profile = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [password, setPassword] = useState("");
    const [retypePassword, setRetypePassword] = useState("");
    const [messageName, setMessageName] = useState("");
    const [messageEmail, setMessageEmail] = useState("");
    const [messagePhoneNumber, setMessagePhoneNumber] = useState("");
    const [messagePassword, setMessagePassword] = useState("");
    const [messageRetypePassword, setMessageRetypePassword] = useState("");
    const username = localStorage.getItem('user_info');
    const [isLoading, setisLoading] = useState(false)
    let flagValid = true;

    useEffect(() => {
        Axios.get(`https://api-tdp-2022.vercel.app/api/profile/${username}`)
            .then(function (response) {
                const { data } = response?.data;
                setName(data.name);
                setEmail(data.email);
                setPhoneNumber(data.phoneNumber);
            }).catch(function (e) {
                console.log("error", e)
            });
    }, [username]);

    const validationName = () => {
        if (!name) {
            setMessageName("Name is required");
            flagValid = false;
        } else {
            setMessageName("");
        }
    }

    const validationEmail = () => {
        if (!email) {
            setMessageEmail("Email is required");
            flagValid = false;
        } else {
            let pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if (!email.match(pattern)) {
                flagValid = false;
                setMessageEmail("Email invalid");
            } else {
                setMessageEmail("")
            }
        }
    }

    const validationPhone = () => {
        if (!phoneNumber) {
            setMessagePhoneNumber("Phone number is required");
            flagValid = false;
        } else {
            if (!phoneNumber.match(/^[0-9]{12}$/)) {
                flagValid = false;
                setMessagePhoneNumber("Phone number invalid");
            } else {
                setMessagePhoneNumber("");
            }
        }
    }

    const validationPassword = () => {
        if (!password) {
            setMessagePassword("Password is required");
            flagValid = false;
        } else {
            setMessagePassword("");
        }
    }

    const validationRetypePassword = () => {
        if (!retypePassword) {
            setMessageRetypePassword("Retype Password is required");
            flagValid = false;
        } else if (password !== retypePassword) {
            setMessageRetypePassword("Retype Password invalid");
            flagValid = false;
        } else {
            setMessageRetypePassword("");
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        validationName()
        validationEmail()
        validationPhone()
        validationPassword()
        validationRetypePassword()
        if (flagValid) {
            setisLoading(true);
            Axios.put(`https://api-tdp-2022.vercel.app/api/profile/${username}`, { name, email, phoneNumber, password })
                .then(function (response) {
                    const { data } = response
                    alert(data?.message);
                    window.location.reload();
                    setisLoading(false);
                })
                .catch(function (e) {
                    alert(e?.response?.data?.message);
                    setisLoading(false);
                });
        }
    }

    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="contact" >
                <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
                <p>Lets get in touch and talk about your next project.</p>
                <form onSubmit={handleSubmit}>
                    <input className="input section border" type="text" placeholder="Name" name="name" onChange={(e) => setName(e?.target?.value)} value={name} />
                    <ErrorMessage item={messageName} />
                    <input className="input section border" type="text" placeholder="Email" name="email" onChange={(e) => setEmail(e?.target?.value)} value={email} />
                    <ErrorMessage item={messageEmail} />
                    <input className="input section border" type="text" placeholder="Phone Number" name="phoneNumber" onChange={(e) => setPhoneNumber(e?.target?.value)} value={phoneNumber} maxLength={12} />
                    <ErrorMessage item={messagePhoneNumber} />
                    <input className="input section border" type="password" placeholder="Password" name="password" onChange={(e) => setPassword(e?.target?.value)} value={password} />
                    <ErrorMessage item={messagePassword} />
                    <input className="input section border" type="password" placeholder="Retype Password" name="retypePassword" onChange={(e) => setRetypePassword(e?.target?.value)} value={retypePassword} />
                    <ErrorMessage item={messageRetypePassword} />
                    <button className="button black section" type="submit" disabled={isLoading}>
                        <i className="fa fa-paper-plane" /> {isLoading ?  'Loading' : 'Update'}
                    </button>
                </form>
            </div >
        </div>
    )
}

export default Profile