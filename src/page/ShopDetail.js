import React from 'react'
import ProductHeader from 'component/ProductHeader'
import ProductInformation from 'component/ProductInformation'

const ShopDetail = () => {
    return (
        <>
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="about">
                    <ProductHeader content='Product Information' />
                    <ProductInformation />
                </div>
            </div>
        </>
    )
}

export default ShopDetail