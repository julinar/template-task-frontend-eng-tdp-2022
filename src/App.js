import 'assets/css/style.css';
import 'assets/css/custom.css';
import { Route, Routes, Navigate } from 'react-router-dom';
import Footer from 'component/Footer'
import NavigationBar from 'component/NavigationBar'
import Homepage from 'page/Homepage';
import Shop from 'page/Shop';
import ShopDetail from 'page/ShopDetail';
import Login from 'page/Login';
import Profile from 'page/Profile';

const App = () => {
    const RequireAuth = ({ children }) => {
        const isLoggedIn = window.localStorage.getItem("isLoggedIn");
        return isLoggedIn ? children : <Navigate to="/login" />
    };

    return (
        <>
            <NavigationBar />
            <Routes>
                <Route path='/' element={<Homepage />} />
                <Route path='/shop' element={<Shop />} />
                <Route path='/shop/:id' element={<ShopDetail />} />
                <Route path='/login' element={<Login />} />
                <Route path='/profile' element={
                    <RequireAuth>
                        <Profile />
                    </RequireAuth>
                } />
            </Routes>
            <Footer />
        </>
    );
}

export default App;
